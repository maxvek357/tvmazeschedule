import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Switch, BrowserRouter, Route } from 'react-router-dom';


ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" component={App} exact />
    </Switch>
  </BrowserRouter>
  , document.getElementById('root'));

