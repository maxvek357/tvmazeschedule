import React from 'react';

import ShowItem from './ShowItem';

import './css/tvshow.css';

const ShowList = ({ day, show }) => {
  const [more, showMore] = React.useState(false);

  console.log("ShowList", more)

  return (
    <section>
      <div className="date">
        {day}
      </div>
      {show.map((item, index) => {
        if (index < 3) {
          return <ShowItem key={item.id} item={item} />
        }
        return null;
      })}
      {more && show.map((item, index) => {
        if (index > 3) {
          return <ShowItem key={item.id} item={item} />
        }
        return null;
      })}
      <div className="mt-4 mb-4">
        <button className="more-button" onClick={() => showMore(!more)}>
          <span className={more ? "active" : ""}>
            {more ? "Hide" : `Load ${show.length - 3} more`}
          </span>
        </button>
      </div>
    </section>
  )
}

export default ShowList;