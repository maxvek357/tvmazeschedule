import React from 'react';

const ShowItem = ({ item }) => {
  return (
    <div className="tvshow-block d-flex mb-3">
      <div className="col-4">
        <img
          src={item.show.image ?
            item.show.image.medium :
            "https://www.nocowboys.co.nz/images/v3/no-image-available.png"}
          alt={item.name} />
      </div>
      <div className="col">
        <ul className="list-unstyled">
          <li className="tvshow-title">
            {item.show.name}
          </li>
          <li className="tvshow-year">
            {item.show.premiered.substr(0, 4)}
          </li>
          <li>
            <div className="tvshow-season-info">
              <ul className="list-inline">
                <li className="list-inline-item">
                  Season: {item.season}
                </li>
                <li className="list-inline-item">
                  Episode: {item.number}
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default ShowItem;