import React from 'react';

import { uniqueId } from 'lodash';

import ShowList from './ShowList';

import './css/tvshow.css';

class Tvshow extends React.Component {
  render() {
    const { show } = this.props;
    let daySection = [];

    if (show && show.length === 0) {
      return (
        <div>no show</div>
      )
    }
    for (let entries in show) {
      console.log("tvshow", entries, show[entries])
      daySection.push(
        <ShowList day={entries} show={show[entries]} key={uniqueId('showlist_')} />
      )
    }

    return (
      <>
        {daySection.map(item => {
          return item
        })}
      </>
    )
  }
}

export default Tvshow;