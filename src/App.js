import React from 'react';

import Calendar from 'react-calendar';

import Tvshow from './components/Tvshow';

import tvimg from './assets/tv.jpg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {
  state = {
    date: new Date(),
    opentvshow: false,
    visible: false,
    showArr: {},
    nextDay: new Date()
  }

  setCorrectDate = date => {
    if (parseInt(date) < 10) {
      return "0" + date;
    }
    return date;
  }

  getDataForUrl = date => {
    const [year, month, day] = [
      date.getFullYear(),
      this.setCorrectDate(date.getMonth()),
      this.setCorrectDate(date.getDate())
    ];
    return [year, month, day];
  }

  getShow = (date, forNextDay = false) => {
    this.setState({ date });

    const [year, month, day] = this.getDataForUrl(date);

    const url = `http://api.tvmaze.com/schedule?country=US&date=${year}-${month}-${day}`;

    fetch(url)
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(res => {
        const key = date.toDateString();
        if (forNextDay) {
          this.setState(prevState => {
            let temp = {
              ...prevState.showArr,
              [key]: res
            };
            let next = new Date();
            next.setDate(date.getDate() + 1);
            return {
              showArr: temp,
              visible: true,
              nextDay: next
            }
          })
        } else {
          this.setState({
            showArr: {
              [key]: res
            },
            visible: true
          })
        }
      })
      .catch(err => {
        console.log(err);
        // if (this.isActive) {
        //   this.setState({ isLoading: false });
        // }
      });
  }

  onClickHendler = date => {
    this.getShow(date);
    const tomorrow = new Date();
    tomorrow.setDate(date.getDate() + 1);
    this.getShow(tomorrow, true);
  }

  render() {
    const { date, showArr, visible, nextDay } = this.state;
    return (
      <div className="container-fluid">
        <div className="row header p-3 mb-2 justify-content-center">
          <div className="container">
            <header className="">
              header
              </header>
          </div>
        </div>
        <div className="row">
          <div className="container">
            <div className="row">
              <div className={
                !visible ? "col-12 d-flex flex-column" : "col-md-6 col-sm-12"
              }>
                <div className="info-block">
                  <img src={tvimg} alt="" />
                  <p>For a list of Tv shows, please select the desired month adn day</p>
                </div>
                <Calendar className="mx-auto custom-calendar"
                  onClickDay={this.onClickHendler}
                  value={date} />
              </div>
              {visible &&
                <div className="col-md-6 col-sm-12">
                  <Tvshow
                    show={showArr}
                    nextDay={nextDay}
                    getNewShow={this.getShow} />
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
