import React from 'react';

import Calendar from 'react-calendar';

import './css/calendar.css';

class CalendarPage extends React.Component {
  state = {
    date: new Date(),
  }

  onChange = date => this.setState({ date }, () => this.getShow());

  setCorrectDate = date => {
    if (parseInt(date) < 10) {
      return "0" + date;
    }
    return date;
  }

  getShow = () => {
    const date = this.state.date;
    const [year, month, day] = [date.getFullYear(), this.setCorrectDate(date.getMonth()), this.setCorrectDate(date.getDate())];

    const url = `http://api.tvmaze.com/schedule?country=US&date=${year}-${month}-${day}`;

    fetch(url)
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error('Failed!');
        }
        return res.json();
      })
      .then(res => {
        console.log(res)
        // const questions = resData.data.getQuestions;
        // if (this.isActive) {
        //   const arr = questions.map(item => {
        //     return this.shuffle([item.correctAnswer, ...item.incorrectAnswer]);
        //   })
        //   this.setState({
        //     questions: questions,
        //     questArr: arr,
        //     isLoading: false
        //   });
        // }
      })
      .catch(err => {
        console.log(err);
        // if (this.isActive) {
        //   this.setState({ isLoading: false });
        // }
      });
  }

  render() {
    console.log(this.state);

    return (
      <div>
        <Calendar className="custom-calendar" onClickDay={this.onChange}
          value={this.state.date} />
      </div>
    )
  }
}

export default CalendarPage;